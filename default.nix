final: prev: {
  nostalgia = final.lib.makeScope final.newScope (self: {
    kq = self.callPackage ./games/kq/kq.nix {};
    linapple-pie = self.callPackage ./emulators/linapple-pie {};
  });
}

