{ cmake
, fetchFromGitHub
, lib
, libpng
, lua
, pkg-config
, SDL2
, SDL2_mixer
, stdenv
, tinyxml-2
, zlib
, cheats ? false
, debugging ? false
}: let
  version = "git";
in stdenv.mkDerivation {
  pname = "kq-fork";
  inherit version;
  src = fetchFromGitHub {
    owner = "OnlineCop";
    repo = "kq-fork";
    rev = "daea27cd16855b5aaa550a0a966d52e41ce72c96";
    hash = "sha256-ONiR/iDWNfTNTdbTwWPg0SIJTLvoTc2uP7bHfI0iV64=";
  };
  patches = [
    ./0001-kq.patch
  ];

  nativeBuildInputs = [ cmake pkg-config ];
  buildInputs = [
    libpng
    lua
    SDL2
    SDL2_mixer
    tinyxml-2
    zlib
  ];

  cmakeFlags = [
    "-DKQ_DATADIR=${placeholder "out"}/share/kq-fork"
    "-DKQ_CHEATS=${if cheats then "1" else "0"}"
    "-DKQ_DEBUGGING=${if debugging then "1" else "0"}"
  ];
}
