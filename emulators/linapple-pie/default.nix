{ gnumake
, fetchFromGitHub
, stdenv
, gnused
, curl
, libzip
, SDL
, SDL_image
, zlib
}: stdenv.mkDerivation rec {
  pname = "linapple-pie";
  version = "git";

  src = fetchFromGitHub {
    owner = "dabonetn";
    repo = "linapple-pie";
    rev = "c22ba0c3ad6c317b4f13486b7ff06a340c831122";
    hash = "sha256-XxrEf4y01z+79yE7pWe2Q6KB+PLh9hzhjD4z4TTjTQo=";
  };
  patches = [
    ./0001-launch-script-copy-conf.patch
  ];

  postPatch = ''
    ${gnused}/bin/sed -i s~/usr/local~$out~g src/Makefile
  '';

  # linapple-pie breaks convention by expecting `make` to be run somewhere
  # besides the project root.
  buildPhase = ''
    pushd src
    make all
    popd
  '';

  # As per above comment + create missing directories.
  installPhase = ''
    mkdir $out
    mkdir $out/bin
    pushd src
    make install
    popd
  '';

  nativeBuildInputs = [
    gnumake
  ];
  buildInputs = [
    (curl.override {opensslSupport = true; zlibSupport = true;})
    libzip
    SDL
    SDL_image
    zlib
  ];
}
