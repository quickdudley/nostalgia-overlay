# Nostalgia Overlay

A nixpkgs overlay for nostalgic games and possibly other software; contained
within a scope called "nostalgia".

## Current contents:

- `nostalgia.kq` provides [`kq-fork`](https://github.com/OnlineCop/kq-fork); the most recently active fork
  of the abandoned game [KQ](https://sourceforge.net/projects/kqlives/)

